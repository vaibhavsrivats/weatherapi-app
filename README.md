# WeatherAPI app

Provide weather forecast for any city

Built using free API service from openweathermap.org
The application has been deployed to Heroku at https://limitless-bayou-15413.herokuapp.com/

The webApp uses Nodejs as a backend to serve templaltes using Express.


#To run the app locally

## Install dependencies
`npm install`

##Run application on localhost
`node app.js`

Then navigate to http://localhost:4000
