var request = require('request');
    express = require("express");
    bodyParser = require("body-parser");
    app = express();

app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended:true }));

var apiKey = '0e8c74374b283fb4a877c3ddb186b9df';
    list = [];
    city = '';
    name = "";


app.get("/", function(req, res){
	res.redirect("/search");
});


//SEARCH route
app.get("/search", function(req, res){

  city = "delhi";       //default city
  url = `http://api.openweathermap.org/data/2.5/forecast?q=${city}&units=metric&appid=${apiKey}`

  request(url, function (err, response, body) {
    if(err){
      console.log('error:', error);
    } else {
      var weather = JSON.parse(body);
      var list = [];
      var name = weather.city.name +', '+ weather.city.country;
      res.render("search", {city: city, weather: weather});
    }
  });
});


//SEARCH POST route
app.post("/search", function(req, res){

  city = req.body.city;
  url = `http://api.openweathermap.org/data/2.5/forecast?q=${city}&units=metric&appid=${apiKey}`

  request(url, function (err, response, body) {
    if(err){
      console.log('error:', err);
    } else {
      var weather = JSON.parse(body);
      var list = [];

      if(typeof weather.city == 'undefined'){
        res.redirect("/");
      } else{
        var name = weather.city.name +', '+ weather.city.country;
        res.render("search", {city: city, weather: weather});
      }
    }
  });
});


//INDEX route
app.get("/weather", function(req, res){

  url = `http://api.openweathermap.org/data/2.5/forecast?q=${city}&units=metric&appid=${apiKey}`;

  request(url, function (err, response, body) {
    if(err){
      console.log('error:', error);
    } else {
      var weather = JSON.parse(body);
      var list = [];
      var name = weather.city.name +', '+ weather.city.country;
      var j=0;
      for(var i=0; weather.list[i+1]!=null; i++){
        if(weather.list[i].dt_txt.split("-")[2].split(" ")[0] != weather.list[(i+1)].dt_txt.split("-")[2].split(" ")[0])
        list[j++] = weather.list[i];
      }
      res.render("index", {list: list, name: name});
    }
  });
});


//GRAPH route
app.get("/graph", function(req,res){

  url = `http://api.openweathermap.org/data/2.5/forecast?q=${city}&units=metric&appid=${apiKey}`;

  request(url, function (err, response, body) {
    if(err){
      console.log('error:', error);
    } else {
      var weather = JSON.parse(body);
      var list = [];
      var name = weather.city.name +', '+ weather.city.country;
      var j=0;
      for(var i=0; weather.list[i+1]!=null; i++){
        if(weather.list[i].dt_txt.split("-")[2].split(" ")[0] != weather.list[(i+1)].dt_txt.split("-")[2].split(" ")[0])
          list[j++] = weather.list[i];
      }
      res.render("graph",{name: name, list: list});
    }
  });
});

app.listen(process.env.PORT || 4000, function(){
	console.log("Server is running!");
});
